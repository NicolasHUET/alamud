# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .failedaction import FailedActionEvent
from .go           import EnterPortalEvent, TraversePortalEvent, LeavePortalEvent, MoveEvent
from .look         import LookEvent
from .inspect      import InspectEvent
from .take         import TakeEvent
from .light        import LightOnEvent, LightOffEvent
from .changeprop   import ChangePropEvent
from .open         import OpenEvent, OpenWithEvent
from .close        import CloseEvent, CloseWithEvent
from .type         import TypeEvent
from .inventory    import InventoryEvent
from .info         import InfoEvent
from .drop         import DropEvent, DropInEvent
from .push         import PushEvent
from .reset        import ResetEvent
from .deadaction   import DeadAction
from .teleport     import TeleportEvent
from .narrative    import NarrativeEvent
from .read         import ReadEvent
from .attack       import HitEvent, AttackEvent
from .combat       import CombatEvent
from .speak        import SpeakEvent
from .magic        import MagicEvent, TempusEvent, ArchaviaEvent
from .learn        import LearnEvent
from .life         import LifeRegainEvent
