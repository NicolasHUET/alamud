# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class LearnEvent(Event2):
    NAME = "learn"
    
    def get_event_templates(self):
        return None

    def perform(self):
        self.actor.add_magic(self.object)
