from .event import Event2

class ReadEvent(Event2):
	NAME = "read"
	
	def perform(self):
		self.inform("read")
