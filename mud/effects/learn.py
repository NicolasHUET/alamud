# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect1
from mud.events import LearnEvent

class LearnEffect(Effect1):
    EVENT = LearnEvent

    def make_event(self):
        return self.EVENT(self.engine, self.actor, self.yaml["magic"])
