from .action import Action2
from mud.events import ReadEvent

class ReadAction(Action2):
	EVENT = ReadEvent
	ACTION = "read"
	RESOLVE_OBJECT = "resolve_for_read"
