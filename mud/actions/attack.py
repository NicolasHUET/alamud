# -*- coding: utf-8 -*-
#==============================================================================

from .action import Action2
from mud.events import HitEvent, AttackEvent

class HitAction(Action2):
    EVENT = HitEvent
    RESOLVE_OBJECT = "resolve_for_attack"
    ACTION = "hit"
    
class AttackAction(Action2):
    EVENT = AttackEvent
    RESOLVE_OBJECT = "resolve_for_attack"
    ACTION = "attack"
