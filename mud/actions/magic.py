# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import MagicEvent

class MagicAction(Action2):
    EVENT = MagicEvent
    RESOLVE_OBJECT = "resolve_for_magic"
    ACTION = "magic"
    
    def resolve(self):
        super().resolve()
        if self.error:
            self.error = "cannot-find-magic"
