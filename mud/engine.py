# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

import tornado.ioloop
import threading, queue
import mud.parser
import mud.game
from .events.combat import NewTurnEvent
from .actions.look import LookAction

class Engine(threading.Thread):

    def __init__(self):
        super().__init__(daemon=True)
        self._queue = queue.Queue()
        self.put = self._queue.put
        self.again = True
        rules = mud.game.GAME.module.make_rules()
        self.parser = mud.parser.Parser(self, rules)

    def run(self):
        while self.again:
            task = self._queue.get()
            if task["type"] == "input" and task["player"].is_alive() and task["player"].container().had_arena() and type(self.parser.parse(task["player"], task["text"].strip())[0]) is not LookAction:
                task["player"].container().arena.put(task["player"], task)
                NewTurnEvent(self,task["player"],task["player"].get_life()).execute()
            else :
                self.perform(task)

    def perform(self, task):
        meth = "perform_%s" % task["type"]
        meth = getattr(self, meth)
        meth(task)

    def perform_input(self, task):
        actor = task["player"]
        text  = task["text"].strip()
        actor.send_echo("<pre>%s</pre>" % text)
        if actor.is_alive():
            action,text = self.parser.parse(actor, text)
        else:
            from mud.events import DeadAction
            # this is actually an event, but it also has an execute method
            action = DeadAction(self, actor)
        if action:
            action.execute()
        else:
            actor.send_error("<p>hein?</p>")

    def perform_save(self, task):
        actor = task["player"]
        mud.game.GAME.save()
        actor.send_info("<p>Sauvegarde effectuée!</p>")

    def perform_birth(self, task):
        actor = task["player"]
        if not actor.container() or not actor.is_alive():
            from mud.events.birth import BirthEvent
            BirthEvent(self, actor).execute()

    def perform_reset(self, task):
        actor = task["player"]
        mud.game.GAME.reset()
